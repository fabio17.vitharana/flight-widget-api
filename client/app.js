const tableBody = document.getElementById("table-body");

const getFlight = () => {
    fetch("http://localhost:8000/flights")
        .then((response) => response.json())
        .then((flights) => {
            populateTable(flights);
            console.log(flights);
        })
        .catch((err) => console.error(err));
};

getFlight();

const populateTable = (flights) => {
    for (const flight of flights) {
        const tableRow = document.createElement("tr");
        const tableIcon = document.createElement("td");

        tableIcon.textContent = "✈️";
        tableRow.append(tableIcon); // we put the tableIcon as a child of tabelRow

        const flightDetails = {
            time: flight.departing,
            destination: flight.destination.toUpperCase(),
            flight: flight.flightNumber.shift(),
            gate: flight.gate,
            remarks: flight.status.toUpperCase(),
        };

        for (const flightDetail in flightDetails) {
            const tableCell = document.createElement("td");
            const word = Array.from(flightDetails[flightDetail]);

            for (const [index, letter] of word.entries()) {
                const letterElement = document.createElement("div");

                setTimeout(() => {
                    letterElement.textContent = letter;
                    letterElement.classList.add('flip');
                    tableCell.append(letterElement);

                }, 100 * index);

            }
            tableRow.append(tableCell);
        }

        tableBody.append(tableRow); // we put a tableRow as a child of tableBody
    }
};
